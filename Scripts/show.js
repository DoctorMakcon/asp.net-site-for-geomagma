﻿$(document).ready(function () {
    $("#hrefs").on("click", "a", function (event) {
        event.preventDefault();
        var id = $(this).attr("href");
        var top = $(id).offset().top;
        $("body,html").animate({ scrollTop: top }, 1000);
    });

    $("#navbar-data").find("li").hover(function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeIn();
    },
    function () {
        $(this).find('.dropdown-menu').stop(true, true).delay(200).fadeOut();
    });

    $(".flip").hover(function () {
        $(this).children("div").stop(true, false).show(500);
        $(this).children("p").stop(true,false).hide(500);
        $(this).stop(true, false).animate({ marginRight: "170" }, 700); 
    },
    function () {
        $(this).children("div").delay(800).hide(500);
        $(this).children("p").delay(800).show(500);
        $(this).delay(800).animate({ marginRight: "0" }, 700);
    });

    if ($("#carousel").children("#mycarousel").attr("id") == undefined) {
        $("#mylist").affix({
            offset: { top: 0, bottom: 500 }
        });
    }
    else {
        $("#mylist").affix({
            offset: { top: 245, bottom: 400 }
        });
    }

    $("#mylist").on("affix.bs.affix", function () {
        var width = $("#mylist").css("width");
        $("#mylist").css({ "padding-top": "0px", "width": width });
    });

    $("#mylist").on("affix-top.bs.affix", function () {
        $("#mylist").css({ "padding-top": "50px", "width": "25%" });
    });

    /*$("#phoneform").keydown(function (key) {
        if (key.which == 8)
        {
            var pos = GetCaretPosition();
            return true;
        }
    });*/

    /*function GetCaretPosition() {
    var input = $("#phoneform")[0];
    return input.selectionStart;
    }*/

    $("#feedback-modal").on("hidden.bs.modal", function () {
        $("#phoneform").val("");
        $("#upd").html("");
    });

    $("#feedback-modal").on("shown.bs.modal", function () {
        $.validator.unobtrusive.parse(document);
        $("#phoneform").on("keypress", function (key) {
            if (key.which == 8)
                return true;
            if (key.which < 48 || key.which > 57 || $("#phoneform").val().length == 15)
                return false;
            var input = $("#phoneform");
            if (input.val().length == 5 || input.val().length == 9 || input.val().length == 12)
                input.val(input.val() + " ");
        });
    });

});

function ShowFeedbackWindow(event) {
    $("#feedback-link").click();
    $("#feedback-modal").modal({
        backdrop: true,
        show: true
    });
}

function ImageClick(event) {
    if (!event)
        event = window.event;
    var img = event.target || event.srcElement || event.originalTarget;
    var string = img.src;
    var pos = -1;
    var lastPos = 0;
    while ((pos = string.indexOf("/", pos + 1)) != -1)
        lastPos = pos;
    var href = "/Home/ShowHighestResolution?pictureName=" + string.substring(lastPos + 1);
    $("#picturelink").attr("href", href).click();
}

function ShowPicture() {
    $("#modDialog").modal({
        keyboard: true,
        backdrop: true,
        show: true
    });
}

function ScrollToTop(event) {
    event.preventDefault();
    $("body,html").animate({ scrollTop: 0 }, 'slow');
}

if (($(window).height() + 300) < $(document).height()) {
    $("#top-link").removeClass("hidden").affix({
        offset: { top: 100 }
    });
}

function ShowList(event) {
    if (!event)
        event = window.event;
    var tag = event.target || event.srcElement || event.originalTarget;
    var a = $(tag).find("a");
    if ($(a).next().css("display") == "none")
        $(a).next().show("slow");
    else $(a).next().hide("slow");
    return false;
}

function Show(event) {
    if (!event)
        event = window.event;
    var a = event.target || event.srcElement || event.originalTarget;
    if ($(a).next().css("display") == "none")
        $(a).next().show("slow");
    else $(a).next().hide("slow");
    return false;
}

function CloseCarousel(event) {
    alert("Hi");
}

function AjaxClick(event) {
    if (!event)
        event = window.event;
    var target = event.target || event.srcElement || event.originalTarget;
    var href = $("#updatelink").attr("href");
    var pos = href.indexOf("=");
    if (pos == -1)
        href += "?paginationIndex=";
    else href = href.substring(0, pos + 1);
    if ($(target).html() == "»") {
        if ($("#pag").find("li[class='active']").next().find("a").html() == "»")
            return;
        var index = $("#pag").find("li[class='active']").find("a").html();
        index++;
        href += index;
        $("#pag").find("li[class='active']").removeClass("active").next().addClass("active");
        $("#updatelink").attr("href", href).click();
    } 
    else {
        if ($(target).html() == "«") {
            if ($("#pag").find("li[class='active']").prev().find("a").html() == "«")
                return;
            var index = $("#pag").find("li[class='active']").find("a").html();
            index--;
            href += index;
            $("#pag").find("li[class='active']").removeClass("active").prev().addClass("active");
            $("#updatelink").attr("href", href).click();
        }
        else {
            href += $(target).html();
            $("#pag").find("li").removeClass("active");
            $(target).parent().addClass("active");
            $("#updatelink").attr("href", href).click();
        }
    }
}

function ShowModal(event) {
    if (!event)
        event = window.event;
    var target = event.target || event.srcElement || event.originalTarget;
    $(".flex-container").find("img").attr("src", target.src);
    $("#myModal").modal({
        backdrop: true,
        show: true
    });
}

function UpdateImage(event) {
    if (!event)
        event = window.event;
    var target = event.target || event.srcElement || event.originalTarget;
    var href = "/ShowImageInHighResolution?prevImage=";
    var src = $("#highResolution").find("img").attr("src");
    href += src;
    target = $(target).parent("a");
    if ($(target).hasClass("left"))
        href += "&dir=l";
    else href += "&dir=r";
    $("#updateimage").attr("href", href).click();
}

function LoadImage(event) {
    $("#highResolution").find("b").hide();
}