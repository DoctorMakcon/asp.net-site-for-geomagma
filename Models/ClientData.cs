﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Template.Models
{
    public class ClientData
    {
        [Required(ErrorMessage = "Необходимо ввести ваш номер телефона")]
        [DataType(DataType.PhoneNumber, ErrorMessage = "Not a number")]
        [StringLength(maximumLength:15, MinimumLength = 15, ErrorMessage = "Введен неправильный номер")]
        public string phone { get; set; }
    }
}