﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Template.Models
{
    public class WorkShop
    {
        public string ShopName { get; set; }
        public string ShopPhone { get; set; }
        public double GeoLong { get; set; }
        public double GeoLat { get; set; }
    }
}