﻿using System;
using System.Data;
using System.Text;
using System.Drawing;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using GemBox.Spreadsheet;

namespace Template.ExcelParser
{
    public class ExcelParser
    {
        public ExcelParser()
        {
            SpreadsheetInfo.SetLicense("FREE-LIMITED-KEY");
        }
        public DataTable Parse(int sheetPosition, string way)                //zero based
        {
            ExcelFile file = ExcelFile.Load(way);
            ExcelWorksheet sheet = file.Worksheets[sheetPosition];

            DataTable table = new DataTable();
            for (int i = 0; i < sheet.Columns.Count; i++)
                table.Columns.Add();

            ExtractToDataTableOptions options = new ExtractToDataTableOptions(0, 0, sheet.Rows.Count);
            options.ExtractDataOptions = ExtractDataOptions.SkipEmptyRows;
            options.ExcelCellToDataTableCellConverting += (sender, e) =>
            {
                if (!e.IsDataTableValueValid)
                    e.DataTableValue = e.ExcelCell.Value == null ? null : e.ExcelCell.Value.ToString();
                e.Action = ExtractDataEventAction.Continue;
            };
            sheet.ExtractToDataTable(table, options);
            return table;
        }   
    }
}