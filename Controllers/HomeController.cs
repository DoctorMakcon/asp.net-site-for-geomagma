﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Data;
using System.Configuration;
using System.Net;
using System.Web.Helpers;
using Template.MailController;
using Template.Models;
using System.Threading;

namespace Template.Controllers
{
    //"D://projects//C#//ASP//Work//Template//Files//"
    //D://DZHosts//LocalUser//M123321//www.geomagma.somee.com//Files//
    //C://HostingSpaces//geomagma//geomagma.by//wwwroot//Files//
    public class HomeController : Controller
    {
        private List<string> CreateImageList(string[] array, int? paginationIndex = null)
        {
            List<string> list = new List<string>();
            if (!paginationIndex.HasValue)
            {
                int maxIndex = 0;
                if (array.Length < 8)
                    maxIndex = array.Length;
                else maxIndex = 8;
                for (int i = 0; i < maxIndex; i++)
                {
                    int pos = array[i].LastIndexOf("/");
                    list.Add(array[i].Substring(pos));
                }
                return list;
            }
            else
            {
                for (int i = 0; i < 8; i++)
                {
                    int index = i + (paginationIndex.Value - 1) * 8;
                    try
                    {
                        int pos = array[index].LastIndexOf("/");
                        list.Add(array[index].Substring(pos));
                    }
                    catch (Exception exc)
                    {
                        break;
                    }
                }
                return list;
            }
        }
        private List<string> ConvertFilePath(string[] array)
        {
            List<string> list = new List<string>();
            foreach (string str in array)
                list.Add(str.Substring(str.IndexOf("\\") + 1));
            return list;
        }
        public ActionResult Index()
        {
            ViewBag.name = "Изготовление мебели на заказ, кухни, шкафы-купе, покраска фасадов МДФ";
            return View();
        }

        public ActionResult SendNotification(ClientData data)
        {
            if (Request.IsAjaxRequest())
            {
                if (ModelState.IsValid)
                {
                    var email = new MailController.MailController().Notify("Новая заявка с номера +" + data.phone, "alex@geomagma.by");
                    Thread thread = new Thread(email.Deliver);
                    thread.Start();
                    return PartialView("SendNotification", "Уведомление успешно отправлено!");
                }  
                else return PartialView("SendNotification", "Что-то пошло не так!");
            }
            else return View("Index");
        }

        public ActionResult FeedbackWindow()
        {
            return PartialView("FeedbackWindow");
        }

        public ActionResult Price()
        {
            string way = ConfigurationManager.AppSettings["path"] + "price.xlsx";
            string way_ = ConfigurationManager.AppSettings["path"] + "price_.xlsx";
            ExcelParser.ExcelParser parser = new ExcelParser.ExcelParser();
            DataTable plastic = parser.Parse(0, way);
            ViewBag.Plastic = plastic;
            DataTable face = parser.Parse(1, way);
            ViewBag.Face = face;
            DataTable mdf = parser.Parse(2, way);
            ViewBag.MDF = mdf;
            DataTable other = parser.Parse(3, way);
            ViewBag.Other = other;

            DataTable vaneerC = parser.Parse(0, way_);
            ViewBag.VaneerC = vaneerC;
            DataTable vaneerL = parser.Parse(1, way_);
            ViewBag.VaneerL = vaneerL;
            ViewBag.name = "Цены";
            return View();
        }

        public ActionResult CornerKitchen(string paginationIndex)
        {
            List<string> list = null;
            string[] array = Directory.GetFiles(ConfigurationManager.AppSettings["path"] + "Catalog//Kitchen", "*.jpg");
            if (!Request.IsAjaxRequest())           //if first pag index 
            {
                ViewBag.Count = (array.Length / 8);
                if (array.Length % 8 != 0)
                    ViewBag.Count++;
                list = CreateImageList(array);
                ViewBag.name = "Кухни";
                return View(list);
            }
            else
            {                                   //if need offset
                list = CreateImageList(array, Convert.ToInt32(paginationIndex));
                return PartialView("ShowImageList", list);
            }
        }

        public ActionResult ShowImageInHighResolution(string prevImage, string dir)
        {
            if (string.IsNullOrEmpty(prevImage))
                return PartialView("ShowImageInHighResolution");
            else
            {
                string result = null;
                string image = prevImage.Substring(prevImage.LastIndexOf("-") - 1);
                string path = ConfigurationManager.AppSettings["path"] + "Catalog//";
                char k = image[0];
                switch(image[0])
                {
                    case 'k':
                        path += "Kitchen";
                        break;
                    case 'l':
                        path += "Locker";
                        break;
                    case 'd':
                        path += "DressingRoom";
                        break;
                    case 'h':
                        path += "LivingRoom";
                        break;
                    case 'o':
                        path += "Office";
                        break;
                    case 't':
                        path += "Trade";
                        break;
                }  
                string[] array = Directory.GetFiles(path, "*.jpg");

                image = prevImage.Substring(prevImage.LastIndexOf("-") + 1);
                image = image.Remove(image.Length - 4);
                int pos = Convert.ToInt32(image);
                if (dir == "r")
                {
                    if (pos == array.Length)
                        pos = 0;
                    result = array[pos];
                }
                else
                {
                    if (pos == 1)
                    {
                        pos = array.Length - 1;
                        result = array[pos];
                    }
                    else result = array[pos - 2];
                }
                result = result.Substring(result.LastIndexOf("/"));
                return PartialView("ShowImageInHighResolution", result);
            }
        }

        public ActionResult DressingRoom(string paginationIndex)
        {
            List<string> list = null;
            string[] array = Directory.GetFiles(ConfigurationManager.AppSettings["path"] + "Catalog//DressingRoom", "*.jpg");
            if (!Request.IsAjaxRequest())
            {
                ViewBag.Count = (array.Length / 8);
                if (array.Length % 8 != 0)
                    ViewBag.Count++;
                list = CreateImageList(array);
                ViewBag.name = "Гардеробные";
                return View(list);
            }
            else
            {
                list = CreateImageList(array, Convert.ToInt32(paginationIndex));
                return PartialView("ShowImageList", list);
            }
        }

        public ActionResult LivingRoom(string paginationIndex)
        {
            List<string> list = null;
            string[] array = Directory.GetFiles(ConfigurationManager.AppSettings["path"] + "Catalog//LivingRoom", "*.jpg");
            if (!Request.IsAjaxRequest())
            {
                ViewBag.Count = (array.Length / 8);
                if (array.Length % 8 != 0)
                    ViewBag.Count++;
                list = CreateImageList(array);
                ViewBag.name = "Гостиные";
                return View(list);
            }
            else
            {
                list = CreateImageList(array, Convert.ToInt32(paginationIndex));
                return PartialView("ShowImageList", list);
            }
        }

        public ActionResult Locker(string paginationIndex)
        {
            List<string> list = null;
            string[] array = Directory.GetFiles(ConfigurationManager.AppSettings["path"] + "Catalog//Locker", "*.jpg");
            if (!Request.IsAjaxRequest())
            {
                ViewBag.Count = (array.Length / 8);
                if (array.Length % 8 != 0)
                    ViewBag.Count++;
                list = CreateImageList(array);
                ViewBag.name = "Шкафы-купе";
                return View(list);
            }
            else
            {
                list = CreateImageList(array, Convert.ToInt32(paginationIndex));
                return PartialView("ShowImageList", list);
            }
        }

        public ActionResult Office(string paginationIndex)
        {
            List<string> list = null;
            string[] array = Directory.GetFiles(ConfigurationManager.AppSettings["path"] + "Catalog//Office", "*.jpg");
            if (!Request.IsAjaxRequest())
            {
                ViewBag.Count = (array.Length / 8);
                if (array.Length % 8 != 0)
                    ViewBag.Count++;
                list = CreateImageList(array);
                ViewBag.name = "Офисная мебель";
                return View(list);
            }
            else
            {
                list = CreateImageList(array, Convert.ToInt32(paginationIndex));
                return PartialView("ShowImageList", list);
            }
        }

        public ActionResult Trade(string paginationIndex)
        {
            List<string> list = null;
            string[] array = Directory.GetFiles(ConfigurationManager.AppSettings["path"] + "Catalog//Trade", "*.jpg");
            if (!Request.IsAjaxRequest())
            {
                ViewBag.Count = (array.Length / 8);
                if (array.Length % 8 != 0)
                    ViewBag.Count++;
                list = CreateImageList(array);
                ViewBag.name = "Торговая мебель";
                return View(list);
            }
            else
            {
                list = CreateImageList(array, Convert.ToInt32(paginationIndex));
                return PartialView("ShowImageList", list);
            }
        }

        public ActionResult DSPCatalog()
        {
            string[] array = Directory.GetFiles(ConfigurationManager.AppSettings["path"] + "Catalog//DSP", "*.jpg");
            List<string> list = ConvertFilePath(array);
            ViewBag.name = "Каталог ДСП";
            return View(list);
        }

        public ActionResult MDFCatalog()
        {
            string[] array = Directory.GetFiles(ConfigurationManager.AppSettings["path"] + "Catalog//MDF", "*.jpg");
            List<string> list = ConvertFilePath(array);
            ViewBag.name = "Каталог МДФ";
            return View(list);
        }

        public ActionResult TabletopCatalog()
        {
            string[] array = Directory.GetFiles(ConfigurationManager.AppSettings["path"] + "Catalog//Tabletop", "*.jpg");
            List<string> list = ConvertFilePath(array);
            ViewBag.name = "Каталог столешниц";
            return View(list);
        }

        public ActionResult VeneerCatalog()
        {
            string[] array = Directory.GetFiles(ConfigurationManager.AppSettings["path"] + "Catalog//Vaneer", "*.jpg");
            List<string> list = ConvertFilePath(array);
            ViewBag.name = "Каталог шпона";
            return View(list);
        }

        public ActionResult PVC()
        {
            ViewBag.name = "ПВХ";
            return View();
        }

        public ActionResult Facade()
        {
            ViewBag.name = "Фасады";
            return View();
        }

        public JsonResult GetMapData()
        {
            //just one object
            //if more objects need use db
            List<WorkShop> list = new List<WorkShop>();
            WorkShop info = new WorkShop
            {
                ShopName = "Геомагмастиль",
                ShopPhone = "+37529 6733574",
                GeoLong = 53.867180,
                GeoLat = 27.515933
            };
            return Json(info, JsonRequestBehavior.AllowGet);
        }
    }
}