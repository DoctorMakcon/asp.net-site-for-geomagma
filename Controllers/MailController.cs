﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ActionMailer.Net.Mvc;

namespace Template.MailController
{
    public class MailController : MailerBase
    {
        public EmailResult Notify(string message, string email)
        {
            To.Add(email);
            Subject = "Новый заказ";
            MessageEncoding = System.Text.Encoding.UTF8;
            return Email("Notify", message);
        }
    }
}